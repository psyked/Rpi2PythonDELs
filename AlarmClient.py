import Pyro4
import sys

args = sys.argv
args.pop(0)
if len(args) >= 1:
    try:        
        daemon = Pyro4.Proxy("PYRONAME:AlarmDaemon")
        method = args.pop(0)
        ret = daemon.callMethod(method, args)
        if ret is not None: print(ret)
    except:
        print("can't call this method (wrong name ?), is the name server or daemon launched ?")

