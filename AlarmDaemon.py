import sys
import pickle
import Pyro4
import threading
from threading import Thread
import time
import datetime
import os

class Alarm:
    weekOfDays = []
    time = []
    isActivate = False
    args = ""
    script = ""

    def __init__(self):
        for i in range(0, 6):
            self.weekOfDays.append(True)
        self.time = [0, 0]

    def __str__(self):
        days = ""
        for i in range(0, len(self.weekOfDays)):
            if self.weekOfDays[i]:
                days += " %d" % (i)
        return "alarm activated (" + str(self.isActivate) + ") : %dH%dmn, days :" % (self.time[0], self.time[1]) + days

    def changeActivation(self):
        self.isActivate = not self.isActivate

    def disActivate(self):
        self.isActivate = False

    def setTime(self, hour, min):
        self.time[0] = hour
        self.time[1] = min

    def setDays(self, days):
        self.weekOfDays = []
        for i in range(0, 6):
            self.weekOfDays.append(False)
        for day in days:
            self.weekOfDays[day] = True

    def setArguments(self, script, args):
        self.script = script
        self.args = args

    def isInTime(self):
        if self.weekOfDays[time.localtime()[6]]:
            if self.time[0] == time.localtime()[3] and self.time[1] == time.localtime()[4]:
                return True
        return False

    def start(self):
        self.isActivate = True
        newThread = threading.Thread(None, self.loopThread(), None, None)
        newThread.start()

    def stop(self):
        self.isActivate = False

    def loopThread(self):
        while self.isActivate:
            if self.isInTime():
                print("call ", self.script, self.args)
                os.system(self.script + " " + self.args)
                time.sleep(55)
            time.sleep(5)


@Pyro4.expose
class AlarmPyroDaemon:
    """
    Daemon traitant les alarmes grace a pyro4
    liste des fonction :
    - addAlarm(days, hour, action, start) ou days est une liste d'int representant les jours de
    la semaine actives, hour : [hour, min], action ["script","arguments"], start : bool pour l'activer ou non
     - stopDaemon()
     - enableDisableAlarm(index)
     - man
     - listeAlarm
     - deleteAlarm(index)
    """
    pyroDaemon = 0
    listAlarms = []
    threadsList = []

    def man(self):
        return self.__doc__

    def __init__(self, daemon):
        print("initDaemon")
        self.pyroDaemon = daemon
        self.loadConfig()

    def stopDaemon(self):
        print("stopDaemon")
        self.saveConfig()
        for i in self.listAlarms:
            self.listAlarms[i].disActivate()
            self.listAlarms.pop(i)
        self.pyroDaemon.shutdown()

    def addAlarm(self, days, hour, action, start):
        print("addAlarm ", days, hour, action, start)
        alarm = Alarm()
        alarm.setDays(days)
        alarm.setTime(hour[0], hour[1])
        alarm.setArguments(action[0], action[1])
        self.listAlarms.append(alarm)
        self.saveConfig()
        if start: alarm.start()

    def enableDisableAlarm(self, index):
        print("enableDisableAlarm ", index)
        if(index < len(self.listAlarms) and index >= 0):
            self.listAlarms[index].changeActivation()

    def deleteAlarm(self, index):
        print("deleteAlarm : ", index)
        if(index >= 0 and index < len(self.listAlarms)):
            self.listAlarms[index].stop()
            self.listAlarms.pop(index)
            self.saveConfig()
        elif(index == -1):
            for i in range(0, len(self.listAlarms)):
                self.deleteAlarm(0)

    def __str__(self):
        print("toString")
        string = "alarms : \n"
        for i in range(0, len(self.listAlarms)):
            string += str(self.listAlarms[i]) + "\n"
        return string

    def loadConfig(self):
        print("loadConfig")
        try:
            with open("AlarmsConfig.txt", "rb") as myFile:
                myUnPickler = pickle.Unpickler(myFile)
                self.listAlarms = myUnPickler.load()
                myFile.close()
        except:
            print("fail to load 'AlarmsConfig.txt'")

    def saveConfig(self):
        print("saveConfig")
        with open("AlarmsConfig.txt", "wb") as myFile:
            myPickler = pickle.Pickler(myFile)
            myPickler.dump(self.listAlarms)
            myFile.close()

    def stringToList(self, string, type = "NONE"):
        li = string.split("[")[1]
        li = li.split("]")[0]
        li = li.split(",")
        liSpe = []

        for i in range(0, len(li)):
            if type.upper() == "STRING":
                liSpe.append(str(li[i]))
            elif type.upper() == "INT":
                liSpe.append(int(li[i]))
            elif type.upper() == "NONE":
                liSpe.append(li[i])

        return liSpe

    def callMethod(self, methode, arg):
        print("callMethode ",methode, arg)
        try:
            if methode.upper() == "ADDALARM":
                days = self.stringToList(arg[0], "int")
                hours = self.stringToList(arg[1], "int")
                args = self.stringToList(arg[2], "string")
                newThread = Thread(None, self.addAlarm(days, hours, args, bool(arg[3])), None, (), None)
                newThread.start()
                self.threadsList.append(newThread)

            elif methode.upper() == "DELETEALARM":
                newThread = Thread(None, self.deleteAlarm(int(arg[0])), None, (), None)
                newThread.start()
                self.threadsList.append(newThread)

            elif methode.upper() == "MAN":
                return self.man()

            elif methode.upper() == "ENABLEDISABLEALARM":
                self.enableDisableAlarm(int(arg[0]))

            elif methode.upper() == "LISTALARMS":
                return self.__str__()
            elif methode.upper() == "STOPDAEMON":
                self.stopDaemon()
                return "daemonStopped"

        except:
           print("fail to call method ", sys.exc_info()[1])

daemon = Pyro4.Daemon()                # make a Pyro daemon
ns = Pyro4.locateNS()                  # find the name server
uri = daemon.register(AlarmPyroDaemon(daemon))   # register the greeting maker as a Pyro object
ns.register("AlarmDaemon", uri)   # register the object with a name in the name server

print("Alarm Daemon Ready")
daemon.requestLoop()
