﻿"""
Script pour l'utilisation des pins GPIO de raspberry,
l'appel d'une fonction se fait via "script.py -functionName(arguments)".
Les fonctions disponibles sont les suivantes :
changeStatePin(pin)  qui passera la sortie de pin à l'état haut ou bas
pinOn(pin)  qui passera la sortie de pin à l'état haut
pinOff(pin)  qui passera la sortie de pin à l'état bas
twinkle(pin, dureeTotale, dureeHaute, dureeBasse)  qui changera la sortie de pin pendant dureeTotale secondes,
      avec les temps à l'état haut et bas donnés en arguments
PWM(pin, dc, frq)  qui fera une pulsation en sortie de pin de frequence frq et de dutyCycle dc
"""
import Pyro4
import RPi
import RPi.GPIO as gpio
import time
import sys
import threading
from threading import Thread
#todo : change path/name file to unix sys


@Pyro4.expose
class LEDPyroDaemon:
    m_currentNumber = 0
    pyroDaemon = 0
    dictionaryON = {}
    stop = True
    dictionaryPinUsed_Thread = {}
    threadsList = [] #push but not use yet

    def __init__(self, daemon):
        self.pyroDaemon = daemon
        gpio.setmode(gpio.BCM)
        self.stop = False
        #RPi.GPIO.setupwarnings(False)  foutage de gueule sue rpi qui reconnait pas cette methode
        #self.getArguments()

    def stopDaemon(self):
        print("stopDaemon")
        self.stop = True
        for i in self.dictionaryPinUsed_Thread:
            print("one thread to shut down")
            self.dictionaryPinUsed_Thread[i][0] = False
        self.pyroDaemon.shutdown()
        print("pyroDaemon shutted down")


    def man(self):
        print("man:" + __doc__)
        return __doc__

    def getStatePin(self, pin):
        print("getStatePin " + pin)
        return self.dictionaryON.get(pin)

    def getArguments(self):
        """
            :param args: the name of the methode to call with in parentheses, the arguments.
            ex: 'pinOn(12)'  for more, see this file's man
            :return: nothing
            """
        print("getArgs ", sys.argv)
        for element in sys.argv:
            element = element.split("-")
            self.callMethode(str(element))

    def callMethode(self, arg):
        """
        :param args: the name of the methode to call with in parentheses, the arguments.
        ex: 'pinOn(12)'  for more, see this file's man
        :return: nothing
        """
        print("callMethode ", arg)
        element = arg.split("(")
        element[1] = element[1].split(")")
        if element[0].upper() == "CHANGESTATEPIN":
            args = element[1][0]
            args = args.split(",")
            if args is not None:
                newThread = Thread(None, self.changeStatePin(int(args[0])), None, (), None)
                newThread.start()
                self.threadsList.append(newThread)
        elif element[0].upper() == "PINOFF":
            args = element[1][0]
            args = args.split(",")
            if args is not None:
                newThread = Thread(None, self.pinOff(int(args[0])), None, (), None)
                newThread.start()
                self.threadsList.append(newThread)
        elif element[0].upper() == "PINON":
            args = element[1][0]
            args = args.split(",")
            if args is not None:
                newThread = Thread(None, self.pinOn(int(args[0])), None, (), None)
                newThread.start()
                self.threadsList.append(newThread)
        elif element[0].upper() == "TWINKLE":
            args = element[1][0]
            args = args.split(",")
            if args[0] is not None and args[1] is not None and args[2] is not None and args[3] is not None:
                newThread = Thread(None, self.twinkle(int(args[0]), int(args[1]), int(args[2]), int(args[3])), None, (), None)
                newThread.start()
                self.threadsList.append(newThread)
        elif element[0].upper() == "PWM":
            args = element[1][0]
            args = args.split(",")
            if args[0] is not None and args[1] is not None and args[2] is not None and args[3] is not None:
                newThread = Thread(None, self.PWM(int(args[0]), int(args[1]), int(args[2]), int(args[3])), None, (), None)
                newThread.start()
                self.threadsList.append(newThread)
        elif element[0].upper() == "MAN":
            self.man()
        elif element[0].upper() == "STOPDAEMON":
            self.stopDaemon()


    def changeStatePin(self, pin):
        print("changeStatePin : ", pin)
        if self.dictionaryPinUsed_Thread.get(pin) is not None: #if a thread is using the pin
            self.dictionaryPinUsed_Thread[pin][0] = False   #change the condition of the loop of the thread using the pin
            self.dictionaryPinUsed_Thread[pin][1].join() #wait til the end of the thread to change the pin output

        if self.dictionaryON.get(pin) is None:
            print("new entry")
            gpio.setup(pin, gpio.OUT)
            self.dictionaryON[pin] = False
        if self.dictionaryON[pin]:
            self.pinOff(pin)
        else:
            self.pinOn(pin)


    def pinOn(self, pin):
        print("pinON : ", pin)
        if self.dictionaryPinUsed_Thread.get(pin) is not None:  # if a thread is using the pin
            self.dictionaryPinUsed_Thread[pin][0] = False  # change the condition of the loop of the thread using the pin
            self.dictionaryPinUsed_Thread[pin][1].join()  # wait til the end of the thread to change the pin output

        if self.dictionaryON.get(pin) is None:
            gpio.setup(pin, gpio.OUT)
        gpio.output(pin, gpio.HIGH)
        self.dictionaryON[pin] = True


    def pinOff(self, pin):
        print("pinOff : ", pin)
        if self.dictionaryPinUsed_Thread.get(pin) is not None:
            self.dictionaryPinUsed_Thread[pin][0] = False
            self.dictionaryPinUsed_Thread[pin][1].join()

        if self.dictionaryON.get(pin) is None:
            gpio.setup(pin, gpio.OUT)
        self.dictionaryON[pin] = False
        gpio.output(pin, gpio.LOW)


    def twinkle(self, pin, dureeTotale, dureeHaute, dureeBasse):
        print("twinkle pin : " + str(pin) + "for : " + str(dureeTotale) + "with : " + str(dureeHaute) + "/" + str(dureeBasse))
        if self.dictionaryPinUsed_Thread.get(pin) is not None:
            print("pin used")
            self.dictionaryPinUsed_Thread[pin][0] = False
            print("wait for the end of the thread")
            loop = True
            while loop and not self.stop:
                try:
                    print(self.dictionaryPinUsed_Thread[pin])
                except:
                    loop = False
                time.sleep(0.1)
            print("thread stopped")

        if self.dictionaryON.get(pin) is None:
            print("pin not configured")
            gpio.setup(pin, gpio.OUT)
        print("add entry dico")
        self.dictionaryON[pin] = False
        print("add entry pinT")
        self.dictionaryPinUsed_Thread[pin] = [True, threading.current_thread()]
        print("loop begin")
        while self.dictionaryPinUsed_Thread[pin][0] and not self.stop:
            if not self.dictionaryON[pin]:
                print("high")
                gpio.output(pin, gpio.HIGH)
                wait = dureeHaute
            else:
                print("low")
                gpio.output(pin, gpio.LOW)
                wait = dureeBasse
            print("change high/low in dico")
            self.dictionaryON[pin] = not self.dictionaryON[pin]
            time.sleep(wait)
            print("substract time")
            dureeTotale -= wait
            if dureeTotale <= 0:
                self.dictionaryPinUsed_Thread[pin][0] = False
        print("loop end")
        self.dictionaryPinUsed_Thread.pop(pin, None)  # delete the entry, indicate the end of the thread using this pin
        gpio.output(pin, gpio.LOW)
        self.dictionaryON[pin] = False


    def PWM(self, pin, dc, frq, duree):
        print("PWM pin : " + str(pin) + "DutyCycle" + str(dc) + "frequence" + str(frq) + "pendant " + str(duree) + "sec")
        if self.dictionaryPinUsed_Thread.get(pin) is not None:
            print("pin used")
            self.dictionaryPinUsed_Thread[pin][0] = False
            print("wait for the end of the thread")
            loop = True
            while loop and not self.stop:
                try:
                    print(self.dictionaryPinUsed_Thread[pin])
                except:
                    loop = False
                time.sleep(0.1)
            print("thread stopped")

        if self.dictionaryON.get(pin) is None:
            gpio.setup(pin, gpio.OUT)
        self.dictionaryON[pin] = True
        pwm = gpio.PWM(pin, frq)
        pwm.start(dc)
        self.dictionaryPinUsed_Thread[pin] = [True, threading.current_thread()]
        while self.dictionaryPinUsed_Thread[pin][0] and not self.stop:
            time.sleep(0.1)
            duree -= 0.1
            if duree <= 0:
                self.dictionaryPinUsed_Thread[pin][0] = False
        print("end loop")
        self.dictionaryPinUsed_Thread.pop(pin, None)  # delete the entry, indicate the end of the thread using this pin
        pwm.stop()
        self.dictionaryON[pin] = False
        print("end thread")


daemon = Pyro4.Daemon()                # make a Pyro daemon
ns = Pyro4.locateNS()                  # find the name server
uri = daemon.register(LEDPyroDaemon(daemon))   # register the greeting maker as a Pyro object
ns.register("PinDaemon", uri)   # register the object with a name in the name server
print("Pin Daemon Ready")
daemon.requestLoop()                   # start the event loop of the server to wait for calls